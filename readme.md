## Environments

- Nginx
- PHP 7.1
- Postgres latest
- Redis

## Installation guide

- Set up your nginx.conf -> server block, to: 
```
server_name blackbox.dev api.blackbox.dev admin.blackbox.dev;
```
- Add these lines to your /etc/hosts: 

```
127.0.0.1 blackbox.dev
127.0.0.1 api.blackbox.dev
127.0.0.1 admin.blackbox.dev
```
- Change permission of `storage` and `bootstrap\cache` folder
- `composer install --prefer-dist`
- `php artisan key:generate`
- Create postgres database with name "blackbox". If you're using laradock, see below
- Create postgres test database with name "blackbox_test". If you're using laradock, see below
```
docker-compose exec postgres bash
psql -Udefault
CREATE DATABASE blackbox;
"\l" to list all databases
"\d" to list all tables
```
- `php artisan migrate`
- `php artisan db:seed` 
- `php artisan passport:install`
- `npm install`
- `php artisan apiato:docs`

More information here: [apiato.io](http://apiato.io/getting-started/installation/)


## Settings/Config
Access DB manager:
```
blackbox.dev/adminer.php
```

Generate oAuth client:
```
php artisan passport:client --password
Type in: "frontend web"
Then, copy the return value to .env file, at CLIENT_WEB_ID= and CLIENT_WEB_SECRET=
```

Publish api document:
```
php artisan apiato:docs
```

Run automation test:
```
./vendor/bin/phpunit
```

Check code convention:
```
./vendor/bin/phpcs
```

Auto fix code convention:
```
./vendor/bin/phpcbf
```