<?php

namespace App\Containers\SocialAuth\Extra;

/**
 * Class SocialProvider.
 *
 * @author Mahmoud Zalt <mahmoud@zalt.me>
 */
class SocialProvider
{

    const FACEBOOK = 'facebook';
    const TWITTER = 'twitter';
//    CONST GOOGLE = 'google';
}
