<?php
/**
 * Created by PhpStorm.
 * User: phuoc
 * Date: 06/09/2017
 * Time: 23:29
 */

namespace App\Ship\Parents\Traits;

use Ramsey\Uuid\Uuid;

trait Uuids
{
    /**
     * Boot function from laravel.
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = Uuid::uuid4()->toString();
        });
    }
}
